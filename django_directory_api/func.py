import ldap3
from django.conf import settings
from django.template import loader
from django_python3_ldap.conf import settings as ldap_settings

from django_contract_manager_client.django_contract_manager_client.func import (
    contract_manager_get_tenant,
)
from django_kmi_cloud_panel.django_kmi_cloud_panel.models import CpDomains
from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_pwmgr_client.django_pwmgr_client.func import *


def generate_azure_ad_connect_auth_domains(customer: MdatCustomers):
    template = loader.get_template(
        "django_directory_powershell_scripts/azure/azure_ad_connect_auth_domains.ps1"
    )

    template_opts = dict()

    tenant_contract_data = contract_manager_get_tenant(customer.id)

    if (
        not "ms_tenant_domain" in tenant_contract_data
        or not "ms_tenant_id" in tenant_contract_data
    ):
        return ""

    template_opts["ms_tenant_domain"] = tenant_contract_data["ms_tenant_domain"]
    template_opts["ms_tenant_id"] = tenant_contract_data["ms_tenant_id"]

    tenant_domains = CpDomains.objects.filter(
        companycode=customer.org_tag,
    ).exclude(
        domain__endswith=settings.HOSTING_DEFAULT_DOMAIN_BASE,
    )

    template_opts["tenant_domains"] = ", ".join(
        f'"{tenant_domain.domain}"' for tenant_domain in tenant_domains
    )

    return template.render(template_opts)


def generate_azure_ad_sync_users(customer: MdatCustomers):
    template = loader.get_template(
        "django_directory_powershell_scripts/azure/azure_ad_sync_users.ps1"
    )

    template_opts = dict()

    tenant_contract_data = contract_manager_get_tenant(customer.id)

    if (
        not "ms_tenant_domain" in tenant_contract_data
        or not "ms_tenant_id" in tenant_contract_data
    ):
        return ""

    template_opts["ms_tenant_domain"] = tenant_contract_data["ms_tenant_domain"]
    template_opts["ms_tenant_id"] = tenant_contract_data["ms_tenant_id"]

    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    if not ldap_connection.search(
        search_base="ou=" + customer.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectClass=person)(!(objectClass=computer)))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        # no objects
        return list()

    ldap_objects = ldap_connection.response

    tenant_users = list()

    for user in ldap_objects:
        if "codeorange-msol-object-id" not in user["attributes"]:
            continue

        tenant_users.append(
            {
                "object_id": user["attributes"]["codeorange-msol-object-id"][0],
                "upn": user["attributes"]["userPrincipalName"][0],
                "immutable_id": user["attributes"]["codeorange-immutable-id"][0],
            }
        )

    template_opts["tenant_users"] = tenant_users

    return template.render(template_opts)
