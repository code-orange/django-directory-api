from django.urls import path, include
from tastypie.api import Api

from django_directory_api.django_directory_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API TENANT
v1_api.register(TenantResource())

# API USER
v1_api.register(UserResource())
v1_api.register(UserPasswordResource())
v1_api.register(UserGroupsResource())

# API GROUP
v1_api.register(GroupResource())

# API COMPUTER
v1_api.register(ComputerResource())
v1_api.register(ComputerJoinResource())

# API CONTACT
v1_api.register(ContactResource())

# API STATIC
v1_api.register(DirectoryBackendStaticResource())

# API DOMAINS
v1_api.register(AuthDomainResource())

# API SCHEMA
v1_api.register(SchemaAttributeResource())
v1_api.register(SchemaClassResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
