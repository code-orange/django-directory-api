from django.core.management.base import BaseCommand

from django_directory_main.django_directory_main.ldap_connection import ldap_connection
from django_directory_main.django_directory_main.password import change_user_password


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("org_tag", type=str)
        parser.add_argument("key", type=str)
        parser.add_argument("password", type=str)

    def handle(self, *args, **options):
        ldap_con = ldap_connection()

        change_user_password(
            ldap_con,
            org_tag=options["org_tag"],
            key=options["key"],
            new_password=options["password"],
        )
