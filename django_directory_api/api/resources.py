from django.forms import model_to_dict
from tastypie.exceptions import Unauthorized, NotFound, BadRequest
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_directory_main.django_directory_main.ldap_connection import ldap_connection
from django_directory_main.django_directory_main.lookup import *
from django_directory_main.django_directory_main.password import change_user_password
from django_directory_main.django_directory_main.static import *
from django_directory_models.django_directory_models.models import *
from django_kmi_cloud_panel.django_kmi_cloud_panel.views import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)


class DirBeBaseResource(Resource):
    class Meta:
        queryset = None
        always_return_data = True
        allowed_methods = ["get", "post", "delete"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()
        limit = 0
        max_limit = 0


class TenantResource(DirBeBaseResource):
    def obj_get(self, bundle, **kwargs):
        pk_customer = MdatCustomers.objects.get(org_tag=kwargs["pk"])
        customer = bundle.request.user
        sub_customers = customer.get_reseller_customers_and_me()

        if pk_customer not in sub_customers:
            return Unauthorized

        return DirectoryTenantSettings.objects.get(customer_id=pk_customer.id)

    def obj_get_list(self, bundle, **kwargs):
        customer = bundle.request.user

        return DirectoryTenantSettings.objects.filter(
            customer__in=customer.get_reseller_customers_and_me()
        )

    def dehydrate(self, bundle):
        bundle.data = model_to_dict(bundle.obj)

        ldap_con = ldap_connection()

        super_admin_user = lookup_tenant_user(
            ldap_con,
            org_tag=bundle.obj.customer.org_tag,
            key=bundle.obj.super_admin,
        )

        bundle.data["super_admin_upn"] = super_admin_user["userPrincipalName"][0]

        return bundle


class UserResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        result = lookup_tenant_user(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
            key=kwargs["pk"],
        )

        if not result:
            raise NotFound("no such object")

        return result

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        users = lookup_tenant_users(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
        )

        return users

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class UserPasswordResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_create(self, bundle, **kwargs):
        if "key" not in bundle.data:
            raise BadRequest("Add key to your request.")

        if "new_password" not in bundle.data:
            raise BadRequest("Add new_password to your request.")

        ldap_con = ldap_connection()

        change_status = change_user_password(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
            key=bundle.data["key"],
            new_password=bundle.data["new_password"],
        )

        return {"success": change_status}


class UserGroupsResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class GroupResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        result = lookup_tenant_group(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
            key=kwargs["pk"],
        )

        if not result:
            raise NotFound("no such object")

        return result

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        groups = lookup_tenant_groups(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
        )

        return groups

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class ComputerResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        result = lookup_tenant_computer(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
            key=kwargs["pk"],
        )

        if not result:
            raise NotFound("no such object")

        return result

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        computers = lookup_tenant_computers(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
        )

        return computers

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class ComputerJoinResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class ContactResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        result = lookup_tenant_contact(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
            key=kwargs["pk"],
        )

        if not result:
            raise NotFound("no such object")

        return result

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()

        contacts = lookup_tenant_contacts(
            ldap_con,
            org_tag=bundle.request.user.org_tag,
        )

        return contacts

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class DirectoryBackendStaticResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        data = list()

        for attribute, value in DirectoryBackendStatic.__dict__.items():
            if not attribute.startswith("__"):
                data.append(
                    {
                        "attribute": attribute,
                        "value": value,
                    }
                )

        return data

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class AuthDomainResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        realms = get_data_from_api(
            "/company/{}/domains".format(
                bundle.request.user.org_tag.upper(),
            )
        )["data"]

        return realms

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class SchemaAttributeResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()
        return lookup_attribute_schema(ldap_con)

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle


class SchemaClassResource(DirBeBaseResource):
    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        ldap_con = ldap_connection()
        return lookup_class_schema(ldap_con)

    def dehydrate(self, bundle):
        bundle.data = bundle.obj
        return bundle
